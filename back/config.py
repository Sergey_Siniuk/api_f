from functools import lru_cache

from pydantic import BaseSettings


class Settings(BaseSettings):

    POSTGRES_DB: str
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_HOST: str
    POSTGRES_PORT: str

@lru_cache
def get_settings():
    return Settings()